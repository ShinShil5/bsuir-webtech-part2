package views;

import command_runner.Command;
import database.access.IDataAccessHolder;
import database.models.Book;
import database.models.User;
import interfaces.IPrinter;
import interfaces.IView;
import interfaces.states.IBookFilterState;
import interfaces.states.IPaginationState;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class ViewBooks implements IView {
    private IBookFilterState filterState;
    private IPaginationState paginationState;
    private IDataAccessHolder dataAccessHolder;
    private IPrinter printer;

    public ViewBooks(IBookFilterState filterState, IPaginationState paginationState, IDataAccessHolder dataAccessHolder,
                     IPrinter printer) {
        this.filterState = filterState;
        this.paginationState = paginationState;
        this.dataAccessHolder = dataAccessHolder;
        this.printer = printer;
    }

    @Override
    public void print() {
        this.printBookHeader();
        var printed = 0;
        try {
            printed = this.printBooks();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        this.printPaginationInfo(printed);
        this.printFilter();
    }

    @Command(name = "page-next")
    public void nextPage() {
        this.paginationState.setNextPage();
    }

    @Command(name = "page-prev")
    public void prevPage() {
        this.paginationState.setPreviousPage();
    }

    @Command(name = "page-go")
    public void setPage(int page) {
        this.paginationState.setPage(page);
    }

    @Command(name = "page-set-size")
    public void setPageSize(int pageSize) {
        this.paginationState.setPageSize(pageSize);
    }

    @Command(name = "filter-on")
    public void filterOn() {
        this.filterState.on();
    }

    @Command(name = "filter-off")
    public void filterOff() {
        this.filterState.off();
    }

    @Command(name = "filter-set-name")
    public void filterSetName(String name) {
        this.filterState.setName(name);
    }

    @Command(name = "filter-reset-name")
    public void filterResetName() {
        this.filterState.setName(null);
    }

    @Command(name = "filter-set-author")
    public void filterSetAuthor(String author) {
        this.filterState.setAuthor(author);
    }

    @Command(name = "filter-reset-author")
    public void filterResetAuthor() {
        this.filterState.setAuthor(null);
    }

    @Command(name = "filter-reset")
    public void filterReset() {
        this.filterResetName();
        this.filterResetAuthor();
    }

    private int printBooks() throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        var dbBooks = this.dataAccessHolder.getBooks().getAll();
        var filteredBooks = this.filterState.filter(dbBooks);
        var users = this.dataAccessHolder.getUsers().getAll();
        this.paginationState.setElementsAmount(filteredBooks.length);
        var startPrint = (this.paginationState.getPage() - 1) * this.paginationState.getPageSize();
        var endPrint = this.paginationState.getPage() * this.paginationState.getPageSize();
        var maxI = filteredBooks.length;
        var printed = 0;

        for (var i = startPrint; i < endPrint && i < maxI; ++i) {
            User addedBy = null;
            for (var user : users) {
                if (user.id == filteredBooks[i].addedBy) {
                    addedBy = user;
                    break;
                }
            }
            if (addedBy == null) {
                printer.printLine("Fail to print the books. Book with id - "
                        + filteredBooks[i].id + " has invalid addedBy - " + filteredBooks[i].addedBy + ". The user with id from added by wasn't found.");
            }
            printBookRow(filteredBooks[i], addedBy.email);
            printed++;
        }

        return printed;
    }

    private void printBookHeader() {
        printer.fillWithSpaces(3, "ID");
        printer.fillWithSpaces(10, "name");
        printer.fillWithSpaces(10, "author");
        printer.fillWithSpaces(10, "addedBy");
        printer.fillWithSpaces(10, "type");
        printer.printLine();
    }

    private void printBookRow(Book book, String addedByUser) {
        printer.fillWithSpaces(3, book.id + "");
        printer.fillWithSpaces(10, book.name);
        printer.fillWithSpaces(10, book.author);
        printer.fillWithSpaces(10, addedByUser);
        printer.fillWithSpaces(10, book.type == null ? "N/A" : book.type + "");
        printer.printLine();
    }

    private void printFilter() {
        var filterState = this.filterState.getActive() ? "ON" : "OFF";
        var filterName = this.filterState.getName();
        var name = filterName == null ? "ANY" : filterName;
        var filterAuthor = this.filterState.getAuthor();
        var author = filterAuthor == null ? "ANY" : filterAuthor;
        printer.printLine("filter state: " + filterState + "; name - " + name + "; author - " + author);
    }

    private void printPaginationInfo(int printed) {
        printer.printLine("displayed " + printed + " items");
        printer.printLine("page " + paginationState.getPage() +
                " of " + paginationState.getMaxPage()
                + " pages; " + paginationState.getPageSize() + " items per page");
    }
}
