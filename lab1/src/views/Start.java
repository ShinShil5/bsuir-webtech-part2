package views;

import interfaces.IPrinter;
import interfaces.IView;
import interfaces.states.IAuthState;

public class Start implements IView {
    private IAuthState authState;
    private IPrinter printer;

    public Start(IAuthState authState, IPrinter printer) {
       this.authState = authState;
       this.printer = printer;
    }

    @Override
    public void print() {
        if(authState.isAuthorized()) {
            var currUser = authState.getAuthorizedUser();
            printer.printLine("Current user: " + currUser.email + " " + currUser.role.toString());
        }
    }
}
