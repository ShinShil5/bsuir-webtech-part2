package views;

import interfaces.IView;
import interfaces.IViewManager;

/**
 * Manages currently displayed view
 */
public class ViewManager implements IViewManager {
    private IView activeView;
    private IView previousView;

    @Override
    public IView getActiveView() {
        return activeView;
    }

    @Override
    public void update() {
        if(activeView != null) {
            activeView.print();
        }
    }

    @Override
    public void startView(IView view) {
        if(view != null) {
            previousView = previousView == null ? view : activeView;
            activeView = view;
        }
    }

    @Override
    public void startPreviousView() {
       this.startView(previousView);
    }
}
