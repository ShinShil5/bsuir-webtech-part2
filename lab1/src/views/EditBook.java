package views;

import command_runner.Command;
import database.access.IDataAccessHolder;
import database.models.Book;
import database.models.User;
import interfaces.IEmailUtils;
import interfaces.IPrinter;
import interfaces.IView;
import interfaces.IViewManager;
import interfaces.states.IAuthState;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class EditBook implements IView {
    private Book book;
    private Book originalBook;
    private IDataAccessHolder dataAccessHolder;
    private IPrinter printer;
    private IViewManager viewManager;
    private IAuthState authState;
    private IEmailUtils emailUtils;

    public EditBook(Book book, IDataAccessHolder dataAccessHolder, IPrinter printer, IViewManager viewManager, IAuthState authState, IEmailUtils emailUtils) {
       this.book = book;
       this.dataAccessHolder = dataAccessHolder;
       this.printer = printer;
       this.viewManager = viewManager;
       this.authState = authState;
       this.emailUtils = emailUtils;
    }

    public EditBook(int bookId, IDataAccessHolder dataAccessHolder, IPrinter printer, IViewManager viewManager, IAuthState authState, IEmailUtils emailUtils) throws Exception {
        try {
            this.book = Arrays.stream(dataAccessHolder.getBooks().getAll())
                    .filter(book -> book.id == bookId)
                    .findAny()
                    .orElse(null);
            if (this.book == null) {
                throw new Exception("Fail to find book with id: " + bookId);
            }
            this.originalBook = book;
            this.authState = authState;
            this.dataAccessHolder = dataAccessHolder;
            this.printer = printer;
            this.viewManager = viewManager;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void print() {
        printer.printLine("name: " + book.name);
        printer.printLine("author: " + book.author);
        printer.printLine("type: " + book.type);
    }

    @Command(name = "name")
    public void setName(String name) {
        book.name = name;
    }

    @Command(name = "author")
    public void setAuthor(String author) {
        book.author = author;
    }

    @Command(name = "type")
    public void setType(String type){
        var bookType = type.equals("paper")
                ? Book.Type.PAPER
                : Book.Type.ELECTRONIC;

        book.type = bookType;
    }

    @Command(name = "save")
    public void save() {
        var authUser = this.authState.getAuthorizedUser();
        if(authUser == null || authUser.role != User.Role.ADMINISTRATOR) {
            printer.printLine("Fail to save changes, you should be logged as admin");
        }
        if(book.id > 0) {
            try {
                this.dataAccessHolder.getBooks().update(originalBook, book);
                printer.printLine("Book has been updated");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                book.addedBy = authUser.id;
                this.dataAccessHolder.getBooks().add(book);
                this.emailUtils.broadcast("new book has been added, name - " + book.name + " author - " + book.author);
                printer.printLine("Book has been added to database");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        this.viewManager.startPreviousView();
    }

    @Command()
    public void cancel() {
        printer.printLine("Canceled");
        this.viewManager.startPreviousView();
    }
}
