package database.models;

import java.util.List;

public class User implements IUniqueRowByInteger {
    @Override
    public Integer getUnique() {
        return id;
    }

    public enum Role {
        ADMINISTRATOR,
        USER
    }
    public int id;
    public String name;
    public String email;
    public String password;
    public Role role;
    public List<String> messages;
}
