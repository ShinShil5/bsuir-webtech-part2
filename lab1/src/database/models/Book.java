package database.models;

public class Book implements IUniqueRowByInteger {
    @Override
    public Integer getUnique() {
        return id;
    }

    public enum Type {
        ELECTRONIC, PAPER
    }

    public int id;
    public String name;
    public int addedBy;
    public int suggestedBy;
    public String author;
    public String description;
    public Type type;
}
