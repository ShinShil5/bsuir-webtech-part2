package database.models;

public interface IUniqueRowByInteger {
   Integer getUnique();
}
