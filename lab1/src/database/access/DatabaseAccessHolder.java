package database.access;

import database.models.Book;
import database.models.User;

public class DatabaseAccessHolder implements IDataAccessHolder {
    private IDataAccess<Book> books;
    private IDataAccess<User> users;

    public DatabaseAccessHolder(IDataAccess<Book> books, IDataAccess<User> users) {
        this.books = books;
        this.users = users;
    }

    public IDataAccess<Book> getBooks() {
        return books;
    }

    public IDataAccess<User> getUsers() { return users; }

}
