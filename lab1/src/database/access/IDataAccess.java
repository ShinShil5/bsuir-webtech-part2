package database.access;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface IDataAccess<TDataModel> {
    void add(TDataModel model) throws IllegalAccessException, NoSuchFieldException, IOException, NoSuchMethodException, InstantiationException, InvocationTargetException;

    void update(TDataModel oldModel, TDataModel newModel) throws IllegalAccessException, NoSuchFieldException, IOException;

    void delete(TDataModel model) throws IllegalAccessException, NoSuchFieldException, IOException;

    TDataModel[] getAll() throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
}
