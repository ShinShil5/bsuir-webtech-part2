package database.access;

import database.models.Book;
import database.physical.BaseFileDatabase;
import database.physical.BookFileDatabase;

import java.io.IOException;
import java.util.Arrays;

public class BookDataAccess implements IDataAccess<Book>{
    private BaseFileDatabase database;

    public BookDataAccess() throws IOException {
        database = new BookFileDatabase("book.txt");
    }

    @Override
    public void add(Book book) throws IOException {
        Object[] dbRecords = database.getRecords();
        int bookId = dbRecords.length > 0 ?
                ((Book)dbRecords[dbRecords.length - 1]).id
                : 0;
        book.id = bookId + 1;
        database.add(book);
    }

    @Override
    public void update(Book oldBook, Book newBook) throws IllegalAccessException, IOException {
        newBook.id = oldBook.id;
        database.update(oldBook, newBook);
    }

    @Override
    public void delete(Book book) throws IOException {
        database.deleteRecord(book);
    }

    @Override
    public Book[] getAll() throws IOException {
        Object[] tmp = database.getRecords();
        return Arrays.copyOf(tmp, tmp.length, Book[].class);
    }
}
