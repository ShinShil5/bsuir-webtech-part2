package database.access;

import database.models.User;
import database.physical.BaseFileDatabase;
import database.physical.UserFileDatabase;

import java.io.IOException;
import java.util.Arrays;

public class UserDataAccess implements IDataAccess<User> {
    private BaseFileDatabase database;

    public UserDataAccess() throws IOException {
        database = new UserFileDatabase("user.txt");
    }

    @Override
    public void add(User user) throws IOException {
        Object[] dbRecords = database.getRecords();
        int userId = dbRecords.length > 0 ?
                ((User)dbRecords[dbRecords.length - 1]).id
                : 0;
        user.id = userId + 1;
        database.add(user);
    }

    @Override
    public void update(User oldUser, User newUser) throws IOException, IllegalAccessException {
        newUser.id = oldUser.id;
        database.update(oldUser, newUser);
    }

    @Override
    public void delete(User user) throws IOException {
        database.deleteRecord(user);
    }

    @Override
    public User[] getAll() throws IOException {
        Object[] tmp = database.getRecords();
        return Arrays.copyOf(tmp, tmp.length, User[].class);
    }
}
