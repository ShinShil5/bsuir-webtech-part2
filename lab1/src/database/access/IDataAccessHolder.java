package database.access;

import database.models.Book;
import database.models.User;

public interface IDataAccessHolder {
    IDataAccess<Book> getBooks();
    IDataAccess<User> getUsers();
}
