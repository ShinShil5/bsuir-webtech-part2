package database.physical;

import com.google.gson.Gson;
import database.models.IUniqueRowByInteger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class BaseFileDatabase implements IFileDatabase<IUniqueRowByInteger> {
    protected String fileName;
    protected Gson gson;

    public BaseFileDatabase(String fileName) throws IOException {
        this.fileName = fileName;
        this.gson = new Gson();
        var file = new File(fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
    }


    public IUniqueRowByInteger[] getRecords() throws IOException {
        var json = Files.readString(Path.of(fileName));
        var items = getItemsFromJson(json);

        return items;
    }

    public void add(IUniqueRowByInteger obj) throws IOException {
        var items = new ArrayList<>(Arrays.asList(getRecords()));
        items.add(obj);
        saveChanges(items.toArray(new IUniqueRowByInteger[0]));

    }

    public void update(IUniqueRowByInteger oldObj, IUniqueRowByInteger newObj) throws IOException, IllegalAccessException {
        var records = getRecords();
        var existingRec = findRecord(records, oldObj);
        if (existingRec != null) {
            for (var prop : existingRec.getClass().getDeclaredFields()) {
                prop.set(existingRec, prop.get(newObj));
            }
            saveChanges(records);
        }
    }

    @Override
    public void deleteRecord(IUniqueRowByInteger obj) throws IOException {
        var records = Arrays.stream(getRecords())
                .filter(item -> item.getUnique() != obj.getUnique())
                .toArray();
        saveChanges((IUniqueRowByInteger[]) records);
    }

    protected abstract IUniqueRowByInteger[] getItemsFromJson(String json);

    protected abstract String castItemsToJson(IUniqueRowByInteger[] items);

    private void saveChanges(IUniqueRowByInteger[] items) throws IOException {
        var json = castItemsToJson(items);
        var fileWriter = new FileWriter(fileName);
        fileWriter.write(json);
        fileWriter.close();
    }

    private IUniqueRowByInteger findRecord(IUniqueRowByInteger[] objects, IUniqueRowByInteger object) {
        var record = Arrays.stream(objects)
                .filter(item -> item.getUnique() == object.getUnique())
                .findAny()
                .orElse(null);

        return record;
    }
}

