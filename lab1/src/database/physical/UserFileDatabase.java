package database.physical;

import database.models.IUniqueRowByInteger;
import database.models.User;

import java.awt.*;
import java.io.IOException;
import java.util.Arrays;

public class UserFileDatabase extends BaseFileDatabase {
    public UserFileDatabase(String fileName) throws IOException {
        super(fileName);
    }

    @Override
    protected IUniqueRowByInteger[] getItemsFromJson(String json) {
        var gsonWrapper = gson.fromJson(json, GsonWrapper.class);

        return gsonWrapper == null ?  new GsonWrapper().items : gsonWrapper.items;
    }

    @Override
    protected String castItemsToJson(IUniqueRowByInteger[] items) {
        var gsonWrapper = new GsonWrapper(Arrays.asList(items).toArray(new User[0]));
        var json = gson.toJson(gsonWrapper);

        return json;
    }

    private class GsonWrapper {
        public GsonWrapper(User[] items) {
            this.items = items;
        }

        public GsonWrapper() {
            this(new User[]{});
        }

        public User[] items;
    }

}
