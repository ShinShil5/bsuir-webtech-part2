package database.physical;

import database.models.Book;
import database.models.IUniqueRowByInteger;

import java.io.IOException;
import java.util.Arrays;

public class BookFileDatabase extends BaseFileDatabase {
    public BookFileDatabase(String fileName) throws IOException {
        super(fileName);
    }

    @Override
    protected IUniqueRowByInteger[] getItemsFromJson(String json) {
        var gsonWrapper = gson.fromJson(json, GsonWrapper.class);

        return gsonWrapper == null ?  new GsonWrapper().items : gsonWrapper.items;
    }

    @Override
    protected String castItemsToJson(IUniqueRowByInteger[] items) {
        var gsonWrapper = new GsonWrapper(Arrays.asList(items).toArray(new Book[0]));
        var json = gson.toJson(gsonWrapper);

        return json;
    }

    private class GsonWrapper {
        public GsonWrapper(Book[] items) {
            this.items = items;
        }

        public GsonWrapper() {
            this(new Book[]{});
        }

        public Book[] items;
    }
}
