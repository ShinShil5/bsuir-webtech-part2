package database.physical;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IFileDatabase<TModel> {
    TModel[] getRecords() throws IOException;

    void add(TModel obj) throws IOException;

    void update(TModel oldObj, TModel newObj) throws IOException, NoSuchFieldException, IllegalAccessException;

    void deleteRecord(TModel obj) throws IOException;
}
