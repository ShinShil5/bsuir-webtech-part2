public class Main {
    public static void main(String[] args) {
        var viewManager = Application.getViewManager();
        var commandRunner = Application.getCommandRunner();
        var reader = Application.getReader();
        Application.getPrinter().printLine("Book Library 1.0.0");
        viewManager.startView(Application.getNewStartView());
        while(true) {
            var userInput = reader.readLine();
            if(userInput.equals("exit")) {
                break;
            } else {
               commandRunner.runCommand(userInput);
               viewManager.update();
            }
        }
    }
}
