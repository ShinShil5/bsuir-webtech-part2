package interfaces;

public interface IReader {
    String readLine(String enterMessage);
    String readLine();
}
