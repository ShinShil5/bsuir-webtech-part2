package interfaces;

import java.security.NoSuchAlgorithmException;

public interface IPasswordCoder {
    String encode(String password) throws NoSuchAlgorithmException;
}
