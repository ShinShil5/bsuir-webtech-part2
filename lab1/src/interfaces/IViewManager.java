package interfaces;

public interface IViewManager {
    IView getActiveView();
    void update();
    void startView(IView view);
    void startPreviousView();
}
