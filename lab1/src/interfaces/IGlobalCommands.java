package interfaces;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface IGlobalCommands {
    void help(String methodName);
    void help();
    void login(String userName, String password) throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
    void logout();
    void register(String userName, String password, String role) throws IllegalAccessException, IOException, InstantiationException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException;
}
