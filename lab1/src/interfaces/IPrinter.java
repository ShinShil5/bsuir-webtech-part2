package interfaces;

public interface IPrinter {
    void printLine(String message);

    void printLine();

    void print(String message);

    void printCharacter(String character, int amount);

    void fillWithCharacter(String character, int width, String message);

    void printSpaces(int amount);

    void fillWithSpaces(int width, String message);

}
