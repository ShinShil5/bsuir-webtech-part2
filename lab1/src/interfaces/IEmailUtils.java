package interfaces;

public interface IEmailUtils {
    void sendMessage(String message, String email) throws Exception;

    void broadcast(String message);
}
