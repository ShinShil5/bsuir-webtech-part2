package interfaces.states;

import database.models.Book;

import java.util.List;

public interface IBookFilterState {
    boolean shouldBeShown(Book book);

    Book[] filter(Book[] items);

    void setName(String name);

    void setAuthor(String author);

    String getName();

    String getAuthor();

    void on();

    void off();

    boolean getActive();
}
