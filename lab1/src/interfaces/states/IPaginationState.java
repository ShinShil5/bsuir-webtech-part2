package interfaces.states;

public interface IPaginationState {
    int getPage();
    void setPage(int page);
    void setNextPage();
    void setPreviousPage();
    int getMaxPage();
    void setElementsAmount(int elementsAmount);
    void reset(int maxSize);
    int getPageSize();
    void setPageSize(int pageSize);
}
