package interfaces.states;

import database.models.User;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public interface IAuthState {
    boolean isAuthorized();

    void register(String username, String password, User.Role role) throws IllegalAccessException, InvocationTargetException, IOException, InstantiationException, NoSuchMethodException, NoSuchFieldException;

    void login(String username, String password) throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;

    void logout();

    User getAuthorizedUser();

    void refresh() throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
}
