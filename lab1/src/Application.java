import command_runner.CommandRunner;
import command_runner.ICommandRunner;
import database.access.BookDataAccess;
import database.access.DatabaseAccessHolder;
import database.access.IDataAccessHolder;
import database.access.UserDataAccess;
import database.models.Book;
import interfaces.*;
import interfaces.states.IAuthState;
import state.AuthState;
import state.BookFilterState;
import state.PaginationState;
import stateless.EmailUtils;
import stateless.PasswordCoder;
import views.EditBook;
import views.Start;
import views.ViewBooks;
import views.ViewManager;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 * Very simple implementation of dependency injection
 * through the application any public class created here, and any public class can get any other public class via constructor
 */
public class Application {
    private static IDataAccessHolder dataAccessHolder;
    private static IAuthState authState;
    private static IViewManager viewManager;
    private static ICommandRunner commandRunner;
    private static IEmailUtils emailUtils;
    private static IGlobalCommands globalCommands;
    private static IPrinter printer;
    private static IReader reader;
    private static IView start;
    private static IView viewBooks;
    private static IPasswordCoder passwordCoder;

    public static IGlobalCommands getGlobalCommands() {
        return globalCommands;
    }

    public static ICommandRunner getCommandRunner() {
        return commandRunner;
    }

    public static IViewManager getViewManager() {
        return viewManager;
    }

    public static IAuthState getAuthState() {
        return authState;
    }

    public static IDataAccessHolder getDataAccessHolder() {
        return dataAccessHolder;
    }

    public static IView getStartView() {
        return start;
    }

    public static IView getBookAddView() {
        EditBook editBook;
        try {
            editBook = new EditBook(new Book(), dataAccessHolder, printer, viewManager, authState, emailUtils);
        } catch (Exception ex) {
            printer.printLine(ex.getMessage());
            editBook = null;
        }
        return editBook;
    }

    public static IView getBookEditView(int bookId) {
        EditBook editBook;
        try {
            editBook = new EditBook(bookId, dataAccessHolder, printer, viewManager, authState, emailUtils);
        } catch (Exception ex) {
            printer.printLine(ex.getMessage());
            editBook = null;
        }
        return editBook;
    }

    public static IView getNewStartView() {
        start = new Start(authState, printer);

        return start;
    }

    public static IView getViewBooks() {
        return viewBooks;
    }

    public static IView getNewViewBooks() {
        viewBooks = null;
        var dataAccessHolder = getDataAccessHolder();
        var printer = getPrinter();
        try {
            var booksLength = dataAccessHolder.getBooks().getAll().length;
            var paginationState = new PaginationState(booksLength, printer);
            var filterState = new BookFilterState();
            viewBooks = new ViewBooks(filterState, paginationState, dataAccessHolder, printer);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return viewBooks;
    }

    static {
        try {
            staticInit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static IPrinter getPrinter() {
        return printer;
    }

    public static IReader getReader() {
        return reader;
    }

    private static void staticInit() throws IOException {
        printer = new ConsolePrinter();
        reader = new ConsoleReader(printer);
        passwordCoder = new PasswordCoder();
        dataAccessHolder = new DatabaseAccessHolder(new BookDataAccess(), new UserDataAccess());
        authState = new AuthState(dataAccessHolder.getUsers(), passwordCoder);
        viewManager = new ViewManager();
        emailUtils = new EmailUtils(authState, dataAccessHolder, printer);
        globalCommands = new GlobalCommands(authState, printer, viewManager, dataAccessHolder, emailUtils);
        commandRunner = new CommandRunner(globalCommands, viewManager, printer);
    }
}
