import command_runner.ArgName;
import command_runner.Command;
import database.access.IDataAccessHolder;
import database.models.Book;
import database.models.User;
import interfaces.IEmailUtils;
import interfaces.IGlobalCommands;
import interfaces.IPrinter;
import interfaces.IViewManager;
import interfaces.states.IAuthState;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * Contains global commands, which can be run at any time from any view
 */
public class GlobalCommands implements IGlobalCommands {
    private IAuthState authState;
    private IPrinter printer;
    private IViewManager viewManager;
    private IDataAccessHolder dataAccessHolder;
    private IEmailUtils emailUtils;

    public GlobalCommands(IAuthState authState, IPrinter printer, IViewManager viewManager,
                          IDataAccessHolder dataAccessHolder, IEmailUtils emailUtils) {
        this.authState = authState;
        this.printer = printer;
        this.viewManager = viewManager;
        this.dataAccessHolder = dataAccessHolder;
        this.emailUtils = emailUtils;
    }

    @Command
    public void help() {
        printer.printLine("####HELP####");
        var commandRunner = Application.getCommandRunner();
        commandRunner.help();
        printer.printLine("############");
    }

    @Command
    public void help(@ArgName(name = "commandName") String commandName) {
        printer.printLine("####HELP - " + commandName + "####");
        var commandRunner = Application.getCommandRunner();
        commandRunner.help(commandName);
        printer.printLine("############");
    }

    @Command
    public void login(@ArgName(name = "email") String userName,
                      @ArgName(name = "password") String password) throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        this.authState.login(userName, password);
        if (this.authState.getAuthorizedUser() == null) {
            this.printer.printLine("Failed to authorize");
        } else {
            this.printer.printLine("Authorization success");
        }
    }

    @Command
    public void logout() {
        this.authState.logout();
    }

    @Command
    public void register(@ArgName(name = "email") String userName,
                         @ArgName(name = "password") String password,
                         @ArgName(name = "role") String role) {
        try {
            User.Role userRole;
            if (role.equals("admin")) {
                userRole = User.Role.ADMINISTRATOR;
            } else {
                userRole = User.Role.USER;
            }
            this.authState.register(userName, password, userRole);
            printer.printLine("User has been registered");
        } catch (Exception ex) {
            printer.printLine("Fail to register user");
        }
    }

    @Command(name = "view-books")
    public void viewBooks() {
        var viewBooks = Application.getViewBooks();
        if (viewBooks == null) {
            viewBooks = Application.getNewViewBooks();
        }

        this.viewManager.startView(viewBooks);
    }

    @Command(name = "book-add")
    public void addBook() {
        var addBook = Application.getBookAddView();
        this.viewManager.startView(addBook);
    }

    @Command(name = "book-edit")
    public void editBook(int bookId) {
        var editBook = Application.getBookEditView(bookId);
        this.viewManager.startView(editBook);
    }

    @Command(name = "book-add")
    public void addBookByName(String name) {
        var book = new Book();
        book.name = name;
        try {
            this.dataAccessHolder.getBooks().add(book);
            this.emailUtils.broadcast("new book has been added, name - " + book.name + " author - " + book.author);
            printer.printLine("Book has been added");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Command(name = "book-suggest")
    public void suggestBook(String bookName, String bookAuthor, String adminEmail) {
        String message = "Suggest book {name: " + bookName + "; author: " + bookAuthor + "}";
        try {
            this.emailUtils.sendMessage(message, adminEmail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Command(name = "view-messages")
    public void viewMessages() {
        if (this.authState.isAuthorized() == false) {
            printer.printLine("You should be authorized to execute");
            return;
        }
        var messages = this.authState.getAuthorizedUser().messages;
            printer.printLine("####MESSAGES####");
        if(messages != null) {
            for (var m : messages) {
                printer.printLine(m);
            }
        } else {
            printer.printLine("Inbox is empty");
        }
        printer.printLine("########");
    }

    @Command(name = "view-user")
    public void viewUser() {
        if (!this.authState.isAuthorized()) {
            printer.printLine("You should be authorized to execute");
            return;
        }
        this.viewUser(this.authState.getAuthorizedUser().id);
    }

    @Command(name = "view-user")
    public void viewUser(int userId) {
        User user = null;
        try {
            user = Arrays.stream(this.dataAccessHolder.getUsers().getAll())
                        .filter(it -> it.id == userId)
                        .findAny()
                        .orElse(null);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        if(user == null) {
            printer.printLine("Failt to find user with id " + userId);
            return;
        }

        printer.printLine("id: " + user.id);
        printer.printLine("name: " + user.name);
        printer.printLine("email: " + user.email);
        printer.printLine("role: " + user.role);
    }
}
