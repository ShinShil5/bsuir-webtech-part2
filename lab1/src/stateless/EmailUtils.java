package stateless;

import database.access.IDataAccessHolder;
import database.models.User;
import interfaces.IEmailUtils;
import interfaces.IPrinter;
import interfaces.states.IAuthState;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

public class EmailUtils implements IEmailUtils {
    private IAuthState authState;
    private IDataAccessHolder dataAccessHolder;
    private IPrinter printer;

    public EmailUtils(IAuthState authState, IDataAccessHolder dataAccessHolder, IPrinter printer) {
        this.authState = authState;
        this.dataAccessHolder = dataAccessHolder;
        this.printer = printer;
    }

    @Override
    public void sendMessage(String message, String email) throws Exception {
        if (this.authState.isAuthorized() == false) {
            printer.printLine("You should be authorized to execute");
            throw new Exception("You should be authorized to execute");
        }
        String emailMessage = "Message from " + this.authState.getAuthorizedUser().email
                + ": '" + message + "'";
        var user = Arrays.stream(this.dataAccessHolder.getUsers().getAll())
                .filter(it -> it.email.equals(email))
                .findAny()
                .orElse(null);
        if (user == null) {
            printer.printLine("Fail to find the user with suggested email '" + email + "'");
            throw new Exception("Fail to find the user with suggested email '" + email + "'");
        }
        if (user.messages == null) {
            user.messages = new ArrayList<>();
        }
        user.messages.add(emailMessage);
        this.dataAccessHolder.getUsers().update(user, user);
    }

    @Override
    public void broadcast(String message) {
        User[] users = new User[0];
        try {
            users = this.dataAccessHolder.getUsers().getAll();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        for (var user : users) {
            try {
                sendMessage(message, user.email);
            } catch (Exception ex) {
                break;
            }
        }
    }
}
