package stateless;

import interfaces.IPasswordCoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class PasswordCoder implements IPasswordCoder {
    @Override
    public String encode(String password) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = java.security.MessageDigest.getInstance("MD5");
        byte[] arr = messageDigest.digest(password.getBytes());
        return Base64.getEncoder().encodeToString(arr);
    }
}
