package command_runner;

public interface ICommandRunner {
    void runCommand(String userInput);
    void help();
    void help(String commandName);
}
