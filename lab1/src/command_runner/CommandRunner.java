package command_runner;

import interfaces.IGlobalCommands;
import interfaces.IPrinter;
import interfaces.IViewManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * run command from string input, first word is command name, others are command args
 * each command is a method marked by command attribute
 * commands are searched in global command and currently active view
 */
public class CommandRunner implements ICommandRunner {
    private IGlobalCommands globalCommands;
    private IViewManager viewManager;
    private IPrinter printer;

    private class ConsoleCommandWrapper {
        public Object holder;
        public List<Method> methods;

        public ConsoleCommandWrapper(Object holder, List<Method> methods) {
            this.holder = holder;
            this.methods = methods;
        }
    }

    public CommandRunner(IGlobalCommands globalCommands, IViewManager viewManager, IPrinter printer) {
        this.globalCommands = globalCommands;
        this.viewManager = viewManager;
        this.printer = printer;
    }

    /**
     * run command from userInput
     * tries to find command implementation by name, that suits to provided command args
     * if more than on match, the first command will be executed
     * the activeView has higher priority than globalCommands
     *
     * @param userInput
     */
    @Override
    public void runCommand(String userInput) {
        var commandTokens = userInput.split(" ");
        var commandName = commandTokens[0];
        var commandArgs = Arrays.copyOfRange(commandTokens, 1, commandTokens.length);
        var isInvoked = false;
        var commands = getAllCommandsByName(commandName);
        if(commands.size() > 0) {
            for(var command : commands) {
                isInvoked = InvokeFirstSuitCommand(command, commandArgs);
                if(isInvoked) {
                    break;
                }
            }
            if(!isInvoked) {
               printer.printLine("Argument mismatch, command: " + commandName + ". Enter 'help " + commandName + "' for usage details");
            }
        } else {
            printer.printLine("Unrecognized command, command: " + commandName);
        }
    }

    @Override
    public void help() {
        for(var commandHolder : getAllCommandHolders()) {
            var commands = getObjectCommands(commandHolder);
            for (Method method : commands) {
                printHelpForMethod(method);
            }
        }
    }

    @Override
    public void help(String commandName) {
        var allConsoleCommands = getAllCommandsByName(commandName);
        for(var cmd : allConsoleCommands) {
            for(var method : cmd.methods) {
                printHelpForMethod(method);
            }
        }
    }

    public void printHelpForMethod(Method method) {
        var commandName = getCommandName(method);
        printer.printLine(commandName);
        var parameters = method.getParameters();
        for (var parameter : parameters) {
            var parameterName = getParameterName(parameter);
            printer.printLine("\t" + parameterName + " " + parameter.getType().getCanonicalName());
        }
    }

    private boolean InvokeFirstSuitCommand(ConsoleCommandWrapper consoleCommandWrapper, String[] commandArgs) {
        var isInvoked = false;
        for (var method : consoleCommandWrapper.methods) {
            if (method.getParameterCount() == commandArgs.length) {
                invoke(consoleCommandWrapper.holder, method, commandArgs);
                isInvoked = true;
                break;
            }
        }

        return isInvoked;
    }


    private void invoke(Object commandHolder, Method method, String[] methodArgs) {
        var preparedArgs = new ArrayList<>();
        var parameters = method.getParameters();
        for (int i = 0; i < methodArgs.length; ++i) {
            var parameterName = parameters[i].getType().getCanonicalName();
            Object arg;
            if (parameterName.equals("int")) {
                arg = Integer.parseInt(methodArgs[i]);
            } else if (parameterName.equals("char")) {
                arg = methodArgs[i].charAt(0);
            } else {
                arg = methodArgs[i];
            }
            preparedArgs.add(arg);
        }

        try {
            method.invoke(commandHolder, preparedArgs.toArray());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private List<Object> getAllCommandHolders() {
        ArrayList<Object> methodHolders = new ArrayList<>();
        methodHolders.add(viewManager.getActiveView());
        methodHolders.add(globalCommands);

        return methodHolders;
    }
    private List<ConsoleCommandWrapper> getAllCommandsByName(String commandName) {
        var methodHolders = getAllCommandHolders();
        ArrayList<ConsoleCommandWrapper> results = new ArrayList<>();
        for (var methodHolder : methodHolders) {
            if (methodHolder != null) {
                var methods = getMethodsByName(methodHolder, commandName);
                if(methods.size() > 0) {
                    var command = new ConsoleCommandWrapper(methodHolder, methods);
                    results.add(command);
                }
            }
        }

        return results;
    }

    private List<Method> getMethodsByName(Object methodsHolder, String commandName) {
        var commands = Arrays.stream(methodsHolder.getClass().getMethods())
                .filter(method -> method.isAnnotationPresent(Command.class))
                .filter(method -> {
                    var annotationName = method.getAnnotation(Command.class).name();
                    var name = annotationName.equals("")
                            ? method.getName()
                            : annotationName;
                    return name.equals(commandName);
                })
                .collect(Collectors.toList());

        return commands;
    }


    private List<Method> getObjectCommands(Object methodHolder) {
        var globalCommands = Arrays.stream(methodHolder.getClass().getMethods())
                .filter(method -> method.isAnnotationPresent(Command.class))
                .collect(Collectors.toList());

        return globalCommands;
    }

    private String getParameterName(Parameter parameter) {
        var parameterName = parameter.isAnnotationPresent(ArgName.class)
                ? parameter.getAnnotation(ArgName.class).name()
                : parameter.getName();

        return parameterName;
    }

    private String getCommandName(Method method) {
        var annotationName = method.getAnnotation(Command.class).name();
        var commandName = annotationName.equals("")
                ? method.getName()
                : annotationName;

        return commandName;
    }

}
