import interfaces.IPrinter;
import interfaces.IReader;

import java.util.Scanner;

public class ConsoleReader implements IReader {
    IPrinter printer;
    Scanner scanner;

    public ConsoleReader(IPrinter printer) {
        this.printer = printer;
        this.scanner = new Scanner(System.in);
    }
    @Override
    public String readLine(String enterMessage) {
       this.printer.print(enterMessage);
       var resultString = readLine();

       return resultString;
    }

    @Override
    public String readLine() {
        var resultString = scanner.nextLine();

        return resultString;
    }
}
