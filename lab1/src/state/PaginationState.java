package state;

import interfaces.IPrinter;
import interfaces.states.IPaginationState;

public class PaginationState implements IPaginationState {
    private final int DEFAULT_PAGE = 1;
    private final int DEFAULT_SIZE = 0;
    private final int DEFAULT_PAGE_SIZE = 3;
    private int page;
    private int elementsAmount;
    private int pageSize;
    private IPrinter printer;

    public PaginationState(int maxSize, IPrinter printer) {
        reset(maxSize);
        this.printer = printer;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public void setPage(int page) {
        if(page <= this.getMaxPage() && this.page > 0) {
            this.page = page;
        } else {
           printer.printLine("Bad value for page, value: " + page);
        }
    }

    @Override
    public void setNextPage() {
        this.setPage(this.page + 1);
    }

    @Override
    public void setPreviousPage() {
        this.setPage(this.page - 1);
    }

    @Override
    public int getMaxPage() {
        if(elementsAmount == 0 || elementsAmount == 1) {
            return 1;
        }
        return (elementsAmount - 1) / pageSize + 1;
    }

    @Override
    public void setElementsAmount(int elementsAmount) {
        this.elementsAmount = elementsAmount;
        if(this.getPage() > this.getMaxPage()) {
            this.setPage(this.getMaxPage());
        }
    }

    @Override
    public void reset(int elementsAmount) {
        this.page = DEFAULT_PAGE;
        this.elementsAmount = elementsAmount;
        this.pageSize = DEFAULT_PAGE_SIZE;
    }

    @Override
    public int getPageSize() {
        return this.pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
