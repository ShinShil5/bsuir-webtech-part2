package state;

import database.models.Book;
import interfaces.states.IBookFilterState;

import java.util.Arrays;

public class BookFilterState implements IBookFilterState {
    private String name;
    private String author;
    private boolean isActive;

    public BookFilterState() {
        this.isActive = false;
        this.name = null;
        this.author = null;
    }

    public boolean shouldBeShown(Book book) {
        var isNameMatch = this.name == null || book.name.contains(this.name);
        var isAuthorMatch = this.author == null || book.author.contains(this.author);

        return !isActive || (isNameMatch && isAuthorMatch);
    }

    public Book[] filter(Book[] items) {
        var books = Arrays.stream(items)
                .filter(item -> shouldBeShown(item))
                .toArray(size -> new Book[size]);

        return books;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return this.name;
    }

    public String getAuthor() {
        return this.author;
    }

    @Override
    public void on() {
        isActive = true;
    }

    @Override
    public void off() {
        isActive = false;
    }

    @Override
    public boolean getActive() {
        return isActive;
    }
}
