package state;

import database.access.IDataAccess;
import database.models.User;
import interfaces.IPasswordCoder;
import interfaces.states.IAuthState;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class AuthState implements IAuthState {
    private User authorizedUser = null;
    private IDataAccess<User> userDb;
    private IPasswordCoder passwordCoder;

    public AuthState(IDataAccess<User> userDb, IPasswordCoder passwordCoder) {
        this.userDb = userDb;
        this.passwordCoder = passwordCoder;
    }

    public boolean isAuthorized() {
        return authorizedUser != null;
    }

    public void register(String username, String password, User.Role role) throws IllegalAccessException, InvocationTargetException, IOException, InstantiationException, NoSuchMethodException, NoSuchFieldException {
        var user = new User();
        user.email = username;
        try {
            user.password = passwordCoder.encode(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        };
        user.role = role;
        this.userDb.add(user);
    }

    public void refresh() throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        if(this.authorizedUser != null) {
            var users = this.userDb.getAll();
            var user = Arrays.stream(users)
                    .filter(it -> it.email.equals(authorizedUser.email))
                    .findFirst()
                    .orElse(null);
            this.authorizedUser = user;
        }
    }

    public void login(String username, String password) throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
        var users = this.userDb.getAll();
        var user = Arrays.stream(users)
                .filter(it -> {
                    try {
                        return it.email.equals(username) && it.password.equals(passwordCoder.encode(password));
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    return false;
                })
                .findFirst()
                .orElse(null);
        this.authorizedUser = user;
    }

    public void logout() {
        this.authorizedUser = null;
    }

    public User getAuthorizedUser() {
        return this.authorizedUser;
    }
}
