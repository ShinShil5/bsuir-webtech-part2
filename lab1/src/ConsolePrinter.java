import interfaces.IPrinter;

public class ConsolePrinter implements IPrinter {

    @Override
    public void printLine(String message) {
        print(message + "\n");
    }

    @Override
    public void printLine() {
        printLine("");
    }

    @Override
    public void print(String message) {
        System.out.print(message);
    }

    @Override
    public void printCharacter(String character, int amount) {
        for (int i = 0; i < amount; ++i) {
            print(character);
        }
    }

    @Override
    public void fillWithCharacter(String character, int width, String message) {
        print(message);
        var messageLegth = message == null ? "null".length() : message.length();
        printCharacter(character, width - messageLegth);
    }

    @Override
    public void printSpaces(int amount) {
        printCharacter(" ", amount);
    }

    @Override
    public void fillWithSpaces(int width, String message) {
        fillWithCharacter(" ", width, message);
    }
}
