package lr4.servlets.models;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name="RoomEntity")
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "room", uniqueConstraints = {
        @UniqueConstraint(columnNames = "roomId"),
        @UniqueConstraint(columnNames = "roomNumber") })
public class RoomEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="roomId", unique = true, nullable = false)
    private Integer roomId;

    @Column(name="roomNumber", unique = true, nullable = false)
    private Integer roomNumber;

    @Column(name="roomStateId", unique = true, nullable = false)
    private Integer roomStateId;

    @OneToOne(targetEntity = UserEntity.class)
    @JoinColumn(name = "userId")
    private UserEntity user;

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getRoomStateId() {
        return roomStateId;
    }

    public void setRoomStateId(Integer roomStateId) {
        this.roomStateId = roomStateId;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
