package lr4.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, IOException {
       var httpSession = request.getSession(false);
       if(httpSession == null) {
           response.setStatus(401);
           response.getWriter().print("Session not found");
       } else {
           httpSession.removeAttribute("email");
           httpSession.invalidate();
           response.getWriter().print("Logout success");
       }
    }
}
