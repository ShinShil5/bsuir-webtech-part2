package lr4.servlets;

import com.google.gson.Gson;
import lr4.servlets.lr4.servletes.dto.LoginDto;
import lr4.servlets.models.UserEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, IOException {
        var reader = request.getReader();
        var gson = new Gson();
        var loginDto = gson.fromJson(reader, LoginDto.class);
        var session = lr4.servlets.hibernate.HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        var query = session.getNamedQuery("user.findByEmail");
        query.setString("email", loginDto.getEmail());
        var userEntities = (List<UserEntity>)query.list();
        var writer = response.getWriter();
        if(userEntities.size() == 0) {
            response.setStatus(404);
            writer.print("User not found");
        } else {
            var user = userEntities.get(0);
            var httpSession = request.getSession();
            httpSession.setAttribute("email", user.getEmail());
            writer.print("Login success");
        }
    }
}
