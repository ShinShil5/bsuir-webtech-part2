package lr4.servlets.lr4.servletes.dto;

public class RoomDto {
    private int roomId;
    private String userName;
    private int roomNumber;
    private int roomStateId;

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getRoomStateId() {
        return roomStateId;
    }

    public void setRoomStateId(int roomStateId) {
        this.roomStateId = roomStateId;
    }
}
