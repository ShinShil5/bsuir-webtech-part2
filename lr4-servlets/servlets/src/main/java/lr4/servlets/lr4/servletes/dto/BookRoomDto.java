package lr4.servlets.lr4.servletes.dto;

public class BookRoomDto {
    private int roomId;
    private int roomStateId;
    private int userId;

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getRoomStateId() {
        return roomStateId;
    }

    public void setRoomStateId(int roomStateId) {
        this.roomStateId = roomStateId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
