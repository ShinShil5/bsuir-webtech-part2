package lr4.servlets;

import com.google.gson.Gson;
import lr4.servlets.lr4.servletes.dto.BookRoomDto;
import lr4.servlets.lr4.servletes.dto.RoomDto;
import lr4.servlets.models.RoomEntity;
import lr4.servlets.models.UserEntity;
import org.hibernate.Query;
import org.hibernate.classic.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/rooms")
public class RoomsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, IOException {
        response.setContentType("application/json");
        response.setHeader("X-Version", "3");
        response.setCharacterEncoding("UTF-8");
        List<RoomDto> roomDtos = getRoomDtos();
        PrintWriter out = getPrintWriter(response);
        Gson gson = new Gson();
        String result = gson.toJson(roomDtos);
        out.println(result);
    }

    private PrintWriter getPrintWriter(HttpServletResponse response) throws IOException {
        return response.getWriter();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        var reader = request.getReader();
        var gson = new Gson();
        var bookRoomDto = gson.fromJson(reader, BookRoomDto.class);
        var session = lr4.servlets.hibernate.HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
        var room = (RoomEntity)session.load(RoomEntity.class, bookRoomDto.getRoomId());
        var user = (UserEntity)session.load(UserEntity.class, bookRoomDto.getUserId());
        room.setRoomStateId(bookRoomDto.getRoomStateId());
        room.setUser(user);
        session.update(room);
        session.getTransaction().commit();
        session.close();
        var print = getPrintWriter(response);
        print.print("success");
    }

    private List<RoomDto> getRoomDtos() {
        Session session = lr4.servlets.hibernate.HibernateUtils.getSessionFactory().openSession();
        Query query = session.createQuery("from lr4.servlets.models.RoomEntity");
        List<RoomEntity> rooms = (List<RoomEntity>) query.list();
        return rooms.stream().map(it -> {
            RoomDto roomDto = new RoomDto();
            roomDto.setUserName(it.getUser().getEmail());
            roomDto.setRoomId(it.getRoomId());
            roomDto.setRoomStateId(it.getRoomStateId());
            roomDto.setRoomNumber(it.getRoomNumber());
            return roomDto;
        }).collect(Collectors.toList());
    }
}
