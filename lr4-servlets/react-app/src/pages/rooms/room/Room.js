import React from 'react';
import * as classes from './Room.module.scss';
import './Room.module.scss';
import {connect} from 'react-redux';
import {BOOK_ROOM} from "../../../lib/redux/actions/actionTypes";
import {ROOM_STATE_IDS} from "../../../lib/constants";

const bookClassesMap = {
    [ROOM_STATE_IDS.SELECTED]: classes.selected,
    [ROOM_STATE_IDS.NOT_SELECTED]: '',
    [ROOM_STATE_IDS.BUSY]: classes.busy
};

function Room(props) {
    console.log(props);
    const {room, bookRoom} = props;
    const bookTitle = room.state === ROOM_STATE_IDS.BUSY ? `Booked by ${room.reservedBy}` : '';
    const bookClass = bookClassesMap[room.state] || '';
    return (
        <div
             title={bookTitle}
             className={`${classes.room} ${bookClass}`}
             onClick={bookRoom}>
            <p>{room.roomNumber}</p>
        </div>
    );
}

function mapStateToProps(state, props) {
    console.log(state);
    return {
        room: state.rooms.rooms[props.index]
    }
}

function mapDispatchToProps(dispatch, props) {
    return {
        bookRoom: () => dispatch({type: BOOK_ROOM, index: props.index})
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Room);