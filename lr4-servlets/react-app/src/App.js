import React from 'react';
import {Route, Switch} from 'react-router-dom';
import About from './pages/about/About';
import Home from './pages/home/Home';
import Navigation from './components/navigation/Navigation';
import './App.css';
import Auth from "./pages/auth/Auth";
import Rooms from "./pages/rooms/Rooms";

function App() {
    return (
        <div className="App">
            <Navigation/>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/home" exact component={Home}/>
                <Route path="/about" exact component={About}/>
                <Route path="/rooms" exact render={(props) => <Rooms props={props} testProp={"test"}/>}/>
                <Route path="/auth" exact component={Auth}/>
            </Switch>
        </div>
    );
}

export default App;
