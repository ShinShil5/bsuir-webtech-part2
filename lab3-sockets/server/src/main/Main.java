package main;

import server.Server;

public class Main {
    public static void main(String[] args) {
        var server = new Server();
        var thread = new Thread(server);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
