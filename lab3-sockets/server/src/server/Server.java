package server;

import network.models.Request;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    private static Socket clientSocket;
    private static ServerSocket serverSocket;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;
    private RequestHandler requestHandler;

    @Override
    public void run() {
        requestHandler = new RequestHandler();
        try {
            setupConnection();
            while (true) {
                if (!session()) break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            freeResources();
        }
    }

    private boolean session() throws Exception {
        var request = (Request) in.readObject();
        var response = requestHandler.getResponse(request);
        out.writeObject(response);
        var isExit = request.getCommand().equalsIgnoreCase("exit");
        return !isExit;
    }

    private void setupConnection() {
        try {
            serverSocket = new ServerSocket(4004);
            System.out.println("listening on localhost on port 4004...");
            clientSocket = serverSocket.accept();
            System.out.println("client connected");
            initIO();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void freeResources() {
        try {
            closeIO();
            if (clientSocket != null) {
                clientSocket.close();
            }
            System.out.println("client disconnected");
            if (serverSocket != null) {
                serverSocket.close();
            }
            System.out.println("[serverSocket] serverSocket stopped");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeIO() throws IOException {
        if (out != null) {
            out.flush();
            out.close();
        }
        if (in != null) {
            in.close();
        }
    }

    private void initIO() throws IOException {
        out = new ObjectOutputStream(clientSocket.getOutputStream());
        in = new ObjectInputStream(clientSocket.getInputStream());
    }
}
