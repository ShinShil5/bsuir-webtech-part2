package server;

import main.Main;
import network.models.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import xml.IParser;
import xml.domParser.DomParser;
import xml.saxParser.SaxParser;
import xml.staxParser.StaxParser;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class RequestHandler {
    private Map<String, IParser> xmlReaders;
    private final String employeesXmlFileName = "employees.xml";
    private final String employessXsdFileName = "employees.xsd";

    RequestHandler() {
        this.xmlReaders = new HashMap<>();
        xmlReaders.put("sax", new SaxParser());
        xmlReaders.put("stax", new StaxParser());
        xmlReaders.put("dom", new DomParser());
    }

    Response getResponse(Request request) throws Exception {
        var requestCommand = request.getCommand();
        var parser = xmlReaders.get(requestCommand);
        var response = new Response();
        if (parser != null) {
            createResponseParseEmployeesXml(parser, response);
        } else if (!request.getCommand().equalsIgnoreCase("exit")) {
            handleValidationError(response, new ValidationError("Unknown xml parser " + request.getCommand() + ", allowed one of: sax, stax, dom"));
        } else {
            response.setResponseType(ResponseType.Exit);
        }

        return response;
    }

    private void createResponseParseEmployeesXml(IParser parser, Response response) throws Exception {
        var employeesXmlFile = getFile(employeesXmlFileName);
        var validationResult = validateXml(employeesXmlFile);
        if (validationResult.isValid()) {
            handleEmployeeList(parser, response, employeesXmlFile);
        } else {
            handleValidationError(response, validationResult.getValidationError());
        }
    }

    private void handleValidationError(Response response, ValidationError validationError) {
        response.setResponseType(ResponseType.Error);
        response.setData(validationError);
    }

    private void handleEmployeeList(IParser parser, Response response, File employeesXmlFile) throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
        var employeesBufferedReader = new BufferedReader(new FileReader(employeesXmlFile));
        var employees = parser.parse(employeesBufferedReader);
        response.setResponseType(ResponseType.EmployeeList);
        response.setData(employees);
    }

    private File getFile(String fileName) {
        var pathToExecutedJar = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        var tempFile = new File(pathToExecutedJar);
        var currentDirectory = tempFile.getParentFile().getAbsolutePath();
        var pathToFile = currentDirectory + "/" + fileName;
        return new File(pathToFile);
    }

    private ValidationResult validateXml(File xmlFile) throws Exception {
        var schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        var schema = schemaFactory.newSchema(getFile(employessXsdFileName));
        var validator = schema.newValidator();
        try {
            validator.validate(new StreamSource(xmlFile));
        } catch (SAXParseException ex) {
            var details = new ArrayList<String>();
            details.add("Message: " + ex.getMessage());
            details.add("Column Number: " + ex.getLineNumber());
            details.add("Line number: " + ex.getLineNumber());
            var validationError = new ValidationError("XML is invalid", details);
            return new ValidationResult(validationError);
        }

        return new ValidationResult();
    }
}
