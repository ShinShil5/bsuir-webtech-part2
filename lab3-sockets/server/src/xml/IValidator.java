package xml;

import network.models.ValidationResult;

import java.io.BufferedReader;

public interface IValidator {
    ValidationResult validate(BufferedReader reader);
}
