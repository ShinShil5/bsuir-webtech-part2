package xml;

import network.models.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmployeeXmlParser {
    private List<Employee> employeeList = null;
    private Employee employee = null;
    private StringBuilder data = null;

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    private boolean bAge = false;
    private boolean bName = false;
    private boolean bGender = false;
    private boolean bRole = false;
    private boolean bNote = false;

    public void tagOpen(String tagName, Map<String, String> attributes) {
        if (tagName.equalsIgnoreCase("Employee")) {
            employee = new Employee();
            employee.setNotes(new ArrayList<>());
            employee.setId(attributes.get("id"));
            if (employeeList == null) {
                employeeList = new ArrayList<>();
            }
        } else if (tagName.equalsIgnoreCase("name")) {
            bName = true;
        } else if (tagName.equalsIgnoreCase("age")) {
            bAge = true;
        } else if (tagName.equalsIgnoreCase("gender")) {
            bGender = true;
        } else if (tagName.equalsIgnoreCase("role")) {
            bRole = true;
        } else if (tagName.equalsIgnoreCase("note")) {
            bNote = true;
        }
        data = new StringBuilder();
    }

    public void tagClose(String tagName) {
        if (bAge) {
            employee.setAge(Integer.parseInt(data.toString()));
            bAge = false;
        } else if (bName) {
            employee.setName(data.toString());
            bName = false;
        } else if (bRole) {
            employee.setRole(data.toString());
            bRole = false;
        } else if (bGender) {
            employee.setGender(data.toString());
            bGender = false;
        } else if (bNote) {
            employee.getNotes().add(data.toString());
            bNote = false;
        }
        if (tagName.equalsIgnoreCase("Employee")) {
            employeeList.add(employee);
        }
    }

    public void tagContent(String content) {
        data.append(content);
    }
}
