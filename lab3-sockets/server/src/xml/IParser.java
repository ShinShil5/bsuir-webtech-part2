package xml;

import network.models.Employee;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public interface IParser {
    List<Employee> parse(BufferedReader xmlBufferedReader) throws IOException, SAXException, ParserConfigurationException, XMLStreamException;
}
