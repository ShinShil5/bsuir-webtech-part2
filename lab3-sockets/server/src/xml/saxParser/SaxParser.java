package xml.saxParser;

import network.models.Employee;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import xml.IParser;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class SaxParser implements IParser {

    @Override
    public List<Employee> parse(BufferedReader xmlBufferedReader) throws IOException, SAXException, ParserConfigurationException {
        var saxParserFactory = SAXParserFactory.newInstance();
        var saxParser = saxParserFactory.newSAXParser();
        var saxEmployeeHandler = new SaxEmployeeHandler();
        saxParser.parse(new InputSource(xmlBufferedReader), saxEmployeeHandler);

        return saxEmployeeHandler.getEmpList();
    }
}
