package xml.saxParser;

import network.models.Employee;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import xml.EmployeeXmlParser;

import java.util.HashMap;
import java.util.List;

public class SaxEmployeeHandler extends DefaultHandler {
    private EmployeeXmlParser employeeXmlParser = new EmployeeXmlParser();

    List<Employee> getEmpList() {
        return employeeXmlParser.getEmployeeList();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        var attributesMap = new HashMap<String, String>();
        if(attributes != null) {
            for(var i = 0; i<attributes.getLength(); ++i) {
                var attributeName = attributes.getQName(i);
                var attributesValue = attributes.getValue(i);
                attributesMap.put(attributeName, attributesValue);
            }
        }
        employeeXmlParser.tagOpen(qName, attributesMap);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        employeeXmlParser.tagClose(qName);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        employeeXmlParser.tagContent(new String(ch, start, length));
    }
}
