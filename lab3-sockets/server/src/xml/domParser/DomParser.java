package xml.domParser;

import network.models.Employee;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import xml.EmployeeXmlParser;
import xml.IParser;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class DomParser implements IParser {
    @Override
    public List<Employee> parse(BufferedReader xmlBufferedReader) throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
        var documentBuilderFactory = DocumentBuilderFactory.newInstance();
        var documentBuilder = documentBuilderFactory.newDocumentBuilder();
        var document = documentBuilder.parse(new InputSource(xmlBufferedReader));
        var rootElement = document.getDocumentElement();
        var employeeElements = rootElement.getChildNodes();
        var employeeListBuilder = new EmployeeXmlParser();
        for (var i = 0; i < employeeElements.getLength(); ++i) {
            var employeeElement = employeeElements.item(i);
            parseTag(employeeElement, employeeListBuilder);
        }

        return employeeListBuilder.getEmployeeList();
    }

    private void parseTag(Node element, EmployeeXmlParser employeeXmlParser) {
        var attributes = getElementAttributesAsHashMap(element.getAttributes());
        var tagName = element.getNodeName();
        employeeXmlParser.tagOpen(tagName, attributes);
        processTagValue(element, employeeXmlParser);
        employeeXmlParser.tagClose(tagName);
    }

    private void processTagValue(Node element, EmployeeXmlParser employeeXmlParser) {
        var childNodes = element.getChildNodes();
        if (childNodes.getLength() == 0) {
           employeeXmlParser.tagContent(element.getNodeValue());
        } else {
            for(var i = 0; i<childNodes.getLength(); ++i) {
                var childNode = childNodes.item(i);
                parseTag(childNode, employeeXmlParser);
            }
        }
    }

    private HashMap<String, String> getElementAttributesAsHashMap(NamedNodeMap elementAttributes) {
        var attributes = new HashMap<String, String>();
        if(elementAttributes != null) {
            for (var i = 0; i < elementAttributes.getLength(); ++i) {
                var elementAttribute = elementAttributes.item(i);
                attributes.put(elementAttribute.getNodeName(), elementAttribute.getNodeValue());
            }
        }

        return attributes;
    }
}
