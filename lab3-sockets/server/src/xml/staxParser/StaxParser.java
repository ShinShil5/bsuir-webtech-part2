package xml.staxParser;

import network.models.Employee;
import org.xml.sax.SAXException;
import xml.EmployeeXmlParser;
import xml.IParser;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class StaxParser implements IParser {

    @Override
    public List<Employee> parse(BufferedReader xmlBufferedReader) throws IOException, SAXException, ParserConfigurationException, XMLStreamException {
        var factory = XMLInputFactory.newInstance();
        var eventReader = factory.createXMLEventReader(xmlBufferedReader);
        var employeeXmlParser = new EmployeeXmlParser();

        while (eventReader.hasNext()) {
            var event = eventReader.nextEvent();
            switch (event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    var startElement = event.asStartElement();
                    var attributes = getElementAttributes(startElement);
                    employeeXmlParser.tagOpen(startElement.getName().getLocalPart(), attributes);
                    break;
                case XMLStreamConstants.CHARACTERS:
                    var characters = event.asCharacters();
                    employeeXmlParser.tagContent(characters.getData());
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    var endElement = event.asEndElement();
                    employeeXmlParser.tagClose(endElement.getName().getLocalPart());
                    break;
            }
        }

        return employeeXmlParser.getEmployeeList();
    }

    private HashMap<String, String> getElementAttributes(StartElement startElement) {
        var attributes = new HashMap<String, String>();
        var elementAttributes = startElement.getAttributes();
        if (elementAttributes != null) {
            while (elementAttributes.hasNext()) {
                var elementAttribute = elementAttributes.next();
                var elementAttributeName = elementAttribute.getName().getLocalPart();
                var elementAttributeValue = elementAttribute.getValue();
                attributes.put(elementAttributeName, elementAttributeValue);
            }
        }
        return attributes;
    }
}
