package client;

import network.models.*;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class Client implements Runnable {
    private static Socket clientSocket;
    private static ObjectInputStream in;
    private static ObjectOutputStream out;

    @Override
    public void run() {
        System.out.println("[client] Client started");
        try {
            setupConnection();
            while (true) {
                if (!session()) break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            freeResources();
        }
    }

    private boolean session() throws IOException, ClassNotFoundException {
        out.writeObject(getRequest());
        out.flush();
        var response = (Response) in.readObject();
        boolean shouldContinue = true;
        if (response.getResponseType() == ResponseType.EmployeeList) {
            var employees = (List<Employee>) response.getData();
            for (var employee : employees) {
                System.out.println(employee.toString());
            }
        } else if(response.getResponseType() == ResponseType.Exit) {
            shouldContinue = false;
        } else if (response.getResponseType() == ResponseType.Error) {
            var error = (ValidationError)response.getData();
            System.out.println(error.toString());
        }

        return shouldContinue;
    }

    private void setupConnection() throws IOException {
        clientSocket = new Socket("localhost", 4004);
        out = new ObjectOutputStream(clientSocket.getOutputStream());
        in = new ObjectInputStream(clientSocket.getInputStream());
    }

    private void freeResources() {
        try {
            if(clientSocket != null) {
                clientSocket.close();
            }
            System.out.println("Client closed");
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Request getRequest() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter xml-parser type: ");
        var word = reader.readLine();
        var request = new Request();
        request.setCommand(word);
        return request;
    }
}
