import client.Client;

public class Main {
    public static void main(String[] args) {
        var client = new Client();
        var thread = new Thread(client);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
