package network.models;

import java.io.Serializable;

public class Request implements Serializable {
    private String command;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
