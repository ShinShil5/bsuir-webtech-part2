package network.models;

public class ValidationResult {
    private boolean isValid;
    private ValidationError validationError;

    public ValidationResult() {
       this(true, null);
    }

    public ValidationResult(ValidationError validationError) {
        this(false, validationError);
    }

    private ValidationResult(boolean isValid, ValidationError validationError) {
        this.isValid = isValid;
        this.validationError = validationError;
    }

    public boolean isValid() {
        return isValid;
    }

    public ValidationError getValidationError() {
        return validationError;
    }
}
