package network.models;

import java.io.Serializable;
import java.util.List;

public class Employee implements Serializable {
    private String id;
    private String name;
    private String gender;
    private int age;
    private String role;
    private List<String> notes;

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee:: ID=" + this.id + " Name=" + this.name + " Age=" + this.age + " Gender=" + this.gender +
                " Role=" + this.role + "Notes: \n\t" + String.join("\n\t", this.notes);
    }
}

