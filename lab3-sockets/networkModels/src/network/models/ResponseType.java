package network.models;

public enum ResponseType {
    EmployeeList,
    Exit,
    Error
}
