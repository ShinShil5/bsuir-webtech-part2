package network.models;

import java.io.Serializable;

public class Response implements Serializable {
    private ResponseType responseType;
    private Object data;

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
