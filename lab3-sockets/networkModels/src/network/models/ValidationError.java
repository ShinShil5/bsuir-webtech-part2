package network.models;

import java.io.Serializable;
import java.util.List;

public class ValidationError implements Serializable {
    private String reason;
    private List<String> details;

    public ValidationError(String reason) {
        this(reason, null);
    }

    public ValidationError(String reason, List<String> details) {
        this.reason = reason;
        this.details = details;
    }

    @Override
    public String toString() {
        var errorMessage = "ErrorMessage: " + reason;
        var errorDetails = details == null
                ? ""
                : "; Details: " + String.join(", ", details);
        return errorMessage + errorDetails;
    }

    public String getReason() {
        return reason;
    }

    public List<String> getDetails() {
        return details;
    }
}
