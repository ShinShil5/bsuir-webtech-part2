package lab2.inout;

public class ConsolePrinterSynchronized implements IPrinter {
    public synchronized void printLine(String out) {
        print(out + "\n");
    }

    public synchronized void print(String out) {
        System.out.print(out);
    }

    public synchronized void printLines(String[] lines) {
        for(var line : lines) {
           printLine(line);
        }
    }
}
