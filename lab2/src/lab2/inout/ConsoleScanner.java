package lab2.inout;

import java.util.Scanner;

public class ConsoleScanner implements IScanner {
    @Override
    public String scanLine() {
        var scanner = new Scanner(System.in);
        var line = scanner.nextLine();

        return line;
    }
}
