package lab2.inout;

public interface IPrinter {
    void print(String out);
    void printLine(String out);
    void printLines(String[] lines);
}
