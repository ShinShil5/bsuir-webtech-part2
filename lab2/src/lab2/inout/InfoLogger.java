package lab2.inout;

public class InfoLogger implements ILogger {
    private IPrinter printer;

    public InfoLogger(IPrinter printer) {
        this.printer = printer;
    }

    @Override
    public void log(String line) {
       this.printer.print(line);
    }

    @Override
    public void log(String[] lines) {
       this.printer.printLines(lines);
    }
}
