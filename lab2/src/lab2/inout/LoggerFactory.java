package lab2.inout;

import lab2.models.Ship;
import lab2.models.harbor.BasePier;

import java.text.MessageFormat;

public class LoggerFactory {
    IPrinter consolePrinter;
    IPrinter fileInfoPrinter;
    IPrinter fileErrorPrinter;

    private static LoggerFactory instance;

    public static LoggerFactory getInstance() {
       if(instance == null) {
           instance = new LoggerFactory();
       }

       return instance;
    }

    private LoggerFactory() {
        fileInfoPrinter = new FilePrinter("system-info.txt");
        fileErrorPrinter = new FilePrinter("error-info.txt");
        consolePrinter = new ConsolePrinterSynchronized();
    }

    public ILogger getShipLogger(Ship ship) {
        var logPattern = "ship {0}";
        var logName = MessageFormat.format(logPattern, ship.getId());
        var logger = new Logger(logName, consolePrinter);

        return logger;
    }

    public ILogger getSystemLogger() {
        var logName = "system";
        var logger = new Logger(logName, consolePrinter);

        return logger;
    }

    public ILogger getMooringLogger(Ship ship, BasePier pier) {
        var logPattern = "ship {0}, pier {1}";
        var logName = MessageFormat.format(logPattern, ship.getId(), pier.getId());
        var logger = new Logger(logName, consolePrinter);

        return logger;
    }

    public ILogger getPierLogger(BasePier pier) {
        var logPattern = "pier {0}";
        var logName = MessageFormat.format(logPattern, pier.getId());
        var logger = new Logger(logName, consolePrinter);

        return logger;
    }

    public ILogger getInfoLogger() {
        var logger = new InfoLogger(fileInfoPrinter);

        return logger;
    }

    public ILogger getErrorLogger() {
        var logName = "err";
        var logger = new Logger(logName, fileErrorPrinter);

        return logger;
    }
}
