package lab2.inout;

public interface ILogger {
    void log(String line);
    void log(String[] lines);
}
