package lab2.inout;

public interface IScanner {
    String scanLine();
}
