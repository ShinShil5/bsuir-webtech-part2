package lab2.inout;

import java.io.FileWriter;
import java.io.IOException;

public class FilePrinter implements IPrinter {

    private String fileName;

    public FilePrinter(String fileName) {
        this.fileName = fileName;
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileName);
            fileWriter.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized void print(String out) {
        try {
            var fileWriter = new FileWriter(fileName, true);
            fileWriter.write(out);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printLine(String out) {
        out += "\n";
        print(out);
    }

    @Override
    public synchronized void printLines(String[] lines) {
        for (var line : lines) {
            this.printLine(line);
        }
    }
}
