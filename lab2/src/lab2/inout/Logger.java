package lab2.inout;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Logger implements ILogger {
    private String logName;
    private IPrinter printer;

    public Logger(String logName, IPrinter printer) {
        this.logName = logName;
        this.printer = printer;
    }

    @Override
    public void log(String line) {
        var logMessage = this.getLogMessage(line);
        this.printer.printLine(logMessage);
    }

    @Override
    public void log(String[] lines) {
        var messages = new ArrayList<String>();
        for (var line : lines) {
            messages.add(getLogMessage(line));
        }
        var logMessages = messages.toArray(new String[messages.size()]);

        this.printer.printLines(logMessages);
    }

    private String getLogMessage(String line) {
        var logDateTime = LocalDateTime.now();
        var logPattern = "[{0}] {1} - {2}";
        var logMessage = MessageFormat.format(logPattern, logName, line, logDateTime);

        return logMessage;
    }
}
