/**
 * This application for web tech lab2
 */
package lab2;

import lab2.models.system.SeaSystem;

/**
 * Main class  that contains entry point to the program
 */
public class Main {

    /**
     * Entry point for the application
     *
     * @param args command line params
     */
    public static void main(String[] args) {
        // create instance of sea system
        SeaSystem seaSystem = new SeaSystem();

        // start sea system processes work
        seaSystem.start();
    }
}
