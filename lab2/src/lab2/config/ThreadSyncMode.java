package lab2.config;

public enum ThreadSyncMode {
        SYNCHRONIZED,
        SEMAPHORE
}
