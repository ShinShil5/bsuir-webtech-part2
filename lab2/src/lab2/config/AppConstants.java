package lab2.config;

public class AppConstants implements IAppConstants {
    @Override
    public ThreadSyncMode getThreadSyncMode() {
        return ThreadSyncMode.SYNCHRONIZED;
    }
}
