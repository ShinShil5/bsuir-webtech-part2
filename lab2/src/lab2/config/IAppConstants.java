package lab2.config;

public interface IAppConstants {
    public ThreadSyncMode getThreadSyncMode();
}
