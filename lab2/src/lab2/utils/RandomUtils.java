package lab2.utils;

import java.util.Random;

public class RandomUtils implements IRandomUtils{
    private Random random;

    public RandomUtils() {
        this.random = new Random();
    }

    @Override
    public int randomFrom(int... items) {
        var randomIndex = random.nextInt(items.length);
        var randomItem = items[randomIndex];

        return randomItem;
    }
}
