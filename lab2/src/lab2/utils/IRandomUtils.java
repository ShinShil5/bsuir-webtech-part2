package lab2.utils;

public interface IRandomUtils {
    int randomFrom(int... items);
}
