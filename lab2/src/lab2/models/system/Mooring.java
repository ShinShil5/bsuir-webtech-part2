package lab2.models.system;

import lab2.inout.ILogger;
import lab2.models.Ship;
import lab2.models.harbor.BasePier;
import lab2.utils.IRandomUtils;
import lab2.utils.UtilsDependencies;

public class Mooring implements Runnable {
    // ship that is on the mooring
    public Ship ship;

    // pier to which ship has been moored
    public BasePier pier;

    // console logger
    private ILogger logger;

    // system where the mooring has been registered
    private SeaSystem seaSystem;

    // expected time mooring duration
    private int expectedMooringTime;

    // real mooring duration
    private int mooringTime;

    // is all goods has been transferred
    private boolean isTransferFinished;

    // random utils that is used to generate random numbers
    private IRandomUtils randomUtils;

    /**
     * default constructor
     * @param ship ship on the mooring
     * @param pier pier where mooring happened
     * @param seaSystem system where mooring happened
     */
    public Mooring(Ship ship, BasePier pier, SeaSystem seaSystem) {
        this.ship = ship;
        this.pier = pier;
        this.seaSystem = seaSystem;
        this.expectedMooringTime = this.ship.getExpectedMooringTime();
        this.mooringTime = 0;
        this.isTransferFinished = false;
        this.randomUtils = UtilsDependencies.randomUtils;
    }

    // getter for expected mooringTime
    public int getExpectedMooringTime() {
        return expectedMooringTime;
    }

    // getter for mooringTime
    public int getMooringTime() {
        return mooringTime;
    }

    // getter for isTransferFinished
    public boolean getIsTransferFinished() {
        return this.isTransferFinished;
    }

    /**
     * mooring implementation. It is finished once all the goods from ship would be transferred to the port.
     */
    @Override
    public void run() {
        // log information about mooring start to the console
        log("Mooring has been started, expected time " + expectedMooringTime);
        // while goods are left on the ship
        while (ship.goods.size() > 0) {
            // log information about start good transfer
            log("Start good transfer");

            // get time to transfer the good
            var loadTime = ship.goods.get(0).loadTime;
            try {
                // emulate good transfer with thread freeze
                Thread.sleep(loadTime);
            } catch (InterruptedException e) {
                // ignore interrupt exception, expected that stop of the thread can initialized from the system thread
            }
            // increase mooring time on the good transfer time
            mooringTime += loadTime;
            // emulate random problems with good transfer
            mooringTime += randomUtils.randomFrom(0,0,0,1) * 1000;
            // put good the pier's harbor store
            this.pier.harbor.store.putGood(ship.goods.get(0));
            // remove good from the ship
            ship.goods.remove(0);
            // log information to console
            if (ship.goods.size() > 0) {
                log("Finish good transfer");
            } else {
                // now the transfer is finished
                this.isTransferFinished = true;
                // log information about finishing transfer and all gods are transferred
                // the array is used to putt all the messages one by one on the console
                // because logger String[] implementation is thread safe
                log(new String[]{
                    "Finish good transfer",
                    "All goods has been transferred"
                });
                // notify sea system that mooring has been finished
                seaSystem.finishMooring(this);
            }
        }
    }

    // setter for the logger
    public void setLogger(ILogger logger) {
        this.logger = logger;
    }

    // log information with logger if possible
    public void log(String line) {
        if(this.logger != null) {
            this.logger.log(line);
        }
    }

    // log information with logger if possible
    private void log(String[] lines) {
        if(this.logger != null) {
            this.logger.log(lines);
        }
    }
}
