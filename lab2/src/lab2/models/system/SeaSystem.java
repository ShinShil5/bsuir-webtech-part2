package lab2.models.system;

import lab2.config.AppConstants;
import lab2.config.IAppConstants;
import lab2.config.ThreadSyncMode;
import lab2.inout.ILogger;
import lab2.inout.LoggerFactory;
import lab2.models.Ship;
import lab2.models.harbor.BasePier;
import lab2.models.harbor.Harbor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SeaSystem implements Runnable {
    // list of available ports
    private List<Harbor> ports;

    // list of ships in the system
    private List<Ship> ships;

    // list of moorings, including finished moorings
    private List<Mooring> moorings;

    // list of ships that broke mooring rules in the past
    private List<ShipBreakRules> shipBreakRules;

    // logger factory that can be used to create different instances of the loggers
    private LoggerFactory loggerFactory;

    // logger that is used to log information about sea system one time per five seconds to the file
    private ILogger infoLogger;

    // logger that is used to log errors from shipBreakRules to the file
    private ILogger errorLogger;

    // logger that is used to log information from sea system to the console
    private ILogger systemLogger;

    // application constants, for example which implementation should be used
    private IAppConstants appConstants;

    // semaphore that is used to allow only one ship process to search for the pier
        private static Semaphore enterPierSemaphore = new Semaphore(1);

        /**
         * semaphore that is used to allow only one ship process to update shipBreakRules
         */
        private static Semaphore mooringSemaphore = new Semaphore(1);

    /**
     * default constructor, it is used as simplified dependency injection - all interfaces should be initialized
     * only here
     */
    public SeaSystem() {
        this.loggerFactory = LoggerFactory.getInstance();
        this.infoLogger = loggerFactory.getInfoLogger();
        this.errorLogger = loggerFactory.getErrorLogger();
        this.systemLogger = loggerFactory.getSystemLogger();
        this.appConstants = new AppConstants();
    }

    /**
     * start sea system processes
     * it is creates ports, piers and ships and
     * starts the ships threads. Ships are swimming
     * and searching for goods when ships have found
     * all the goods than they are entering to the pier
     */
    public void start() {
        // init ports, piers and ships, and all other class variables except interfaces
        init();

        // start one thread per ship
        for (var i = 0; i < ships.size(); ++i) {
            Thread shipThread = new Thread(ships.get(i));
            shipThread.start();
        }

        // start thread that will log system information to the file once a time
        var logInfoThread = new Thread(this);
        logInfoThread.start();
    }

    /**
     * Ship enters a pier. This method will search for less loadend pier and put the ship to the pier.
     *
     * @param ship ship that should be entered to pier
     * @return pier where which whip has been entered
     */
    public BasePier enterPier(Ship ship) {
        // use synchronised ot semaphore implementation to enter the pier
        var resultingPier = this.appConstants.getThreadSyncMode() == ThreadSyncMode.SYNCHRONIZED
                ? this.enterPierSynchronised(ship)
                : this.enterPierSemaphore(ship);

        // return the pier, where ship has entered
        return resultingPier;
    }

    /**
     * semaphore implementation of enterPier
     *
     * @param ship
     * @return pier with the ship
     */
    private BasePier enterPierSemaphore(Ship ship) {
        // create resulting pier with default value
        BasePier resultingPier = null;

        // lock the semaphore
        try {
            enterPierSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // put the ship to the pier
        resultingPier = enterPierCommonPart(ship);

        // free the semaphore
        enterPierSemaphore.release();

        // return the resultingPier, now the ship is registered inside this pier
        return resultingPier;
    }

    /**
     * synchronised implementation of enterPier
     *
     * @param ship
     * @return pier with the ship
     */
    private BasePier enterPierSynchronised(Ship ship) {
        // create resulting pier with default value
        BasePier resultingPier = null;

        // part that should be allowed only for the one thread
        synchronized (this) {
            // put the ship to the pier
            resultingPier = enterPierCommonPart(ship);
        }

        // return the resultingPier, now the ship is registered inside this pier
        return resultingPier;
    }

    /**
     * Common part for synchronised and semaphore implementations.
     * This method search for less loadend pier and puts the ship to this pier.
     *
     * @param ship
     * @return
     */
    private BasePier enterPierCommonPart(Ship ship) {
        // pier which currently not working with ships
        BasePier freePier = null;

        // pier with least ships in a queue
        BasePier lessLoadendPier = null;

        // pier to which the ship will be entered
        BasePier resultingPier;

        // search for the port to enter
        // check all ports in the system
        for (var port : ports) {
            // check all piers in the port
            for (var pier : port.piers) {
                // if pier is free than stop search
                if (pier.isFree()) {
                    freePier = pier;
                    break;
                   // if lessLoadendPier is not defined ot amount ships in the queue greater than ships in viewing pier
                } else if (lessLoadendPier == null
                        || lessLoadendPier.getShipsInQueue() > pier.getShipsInQueue()) {
                    lessLoadendPier = pier;
                }
            }
        }

        // set free pier if possible otherwise choose less loadend pier
        resultingPier = freePier == null ? lessLoadendPier : freePier;

        // enter ship to the pier
        resultingPier.enter(ship);

        // return resulting pier, ship is already on the pier or on the pier's queue
        return resultingPier;
    }

    /**
     * register mooring
     * @param mooring mooring object should be generated by the pier that registers the mooring
     */
    public void startMooring(Mooring mooring) {
        // add new mooring to the list of moorings
        this.moorings.add(mooring);

        // start mooring thread
        Thread mooringThread = new Thread(mooring);
        mooringThread.start();
    }

    /**
     * finishes one of the moorings from the mooring list should be called when ship is ready to leave the pier
     * @param mooring
     */
    public void finishMooring(Mooring mooring) {
        // remove the ship from the pier
        mooring.pier.finishMooring();

        // log errors if any
        if (mooring.getMooringTime() > mooring.getExpectedMooringTime()) {
            errorLogger.log("Expected time is " + mooring.getExpectedMooringTime() + ", real time is " + mooring.getMooringTime());
            systemLogger.log("Error about mooring has been logged");
        }

        // move ship to the sea and save errors if any
        if(this.appConstants.getThreadSyncMode() == ThreadSyncMode.SYNCHRONIZED) {
            // synchronised implementation
            synchronized (this) {
                // move ship to the sea and save errors if any
                this.finishMooringCommonPart(mooring);
            }
        } else {
            // semaphore implementation
            try {
                // lock semaphore
                mooringSemaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // move ship to the sea and save errors if any
            this.finishMooringCommonPart(mooring);
            // unlock semaphore
            mooringSemaphore.release();
        }
    }

    /**
     * common part for synchronised and semaphore implementations
     * @param mooring mooring that should be finished
     */
    private void finishMooringCommonPart(Mooring mooring) {
        // update ship break rules or create new one if need
        ShipBreakRules breakRule = null;
        for (var shipBreakRule : shipBreakRules) {
            if (shipBreakRule.shipId == mooring.ship.getId()) {
                breakRule = shipBreakRule;
                break;
            }
        }
        if (breakRule == null) {
            breakRule = new ShipBreakRules();
            breakRule.shipId = mooring.ship.getId();
            breakRule.breakRulesAmount = 0;
            shipBreakRules.add(breakRule);
        }

        // update errors information
        if (mooring.getMooringTime() > mooring.getExpectedMooringTime()) {
            breakRule.breakRulesAmount++;
        }

        // log information to the console from mooring logger
        mooring.log("Mooring has been finished expected time " + mooring.getExpectedMooringTime() + ", mooring time " + mooring.getMooringTime());

        // mooring ship leave pier only when all registrations are finished, so lock on the enter and exit from pier
        mooring.ship.leavePier();
    }

    /**
     * builder to create instance of the ship
     * @return
     */
    private Ship getShip() {
        var ship = new Ship(this);
        var shipLogger = loggerFactory.getShipLogger(ship);
        ship.setLogger(shipLogger);

        return ship;
    }

    /**
     * init all the class fields except interfaces
     */
    private void init() {
        ports = List.of(new Harbor(this));
        ships = List.of(
                this.getShip(),
                this.getShip()
        );
        moorings = new ArrayList<>();
        shipBreakRules = new ArrayList<>();
    }

    /**
     * log system information to a file one time per five seconds
     */
    @Override
    public void run() {
        // do the stuff for all the time the application is running
        while (true) {
            // freeze thread for five seconds
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // build message that should be logged to a file
            var lineSeparator = System.getProperty("line.separator");
            var messageSeparator = "================================================";
            var logMessage = messageSeparator + lineSeparator;
            logMessage += "System info log on " + LocalDateTime.now() + lineSeparator;
            var totalShipsOnMooring = 0;
            var totalShipsOnQueue = 0;
            for (var port : ports) {
                logMessage += "Port 1" + lineSeparator;
                logMessage += "\tGoods on store: " + port.store.goods.size() + lineSeparator;
                for (var pier : port.piers) {
                    var shipsOnMooring = pier.isFree() ? 0 : 1;
                    var shipsOnQueue = pier.getShipsInQueue();
                    logMessage += "\tPier " + pier.getId() + "\n";
                    logMessage += "\t\tShips on mooring: " + shipsOnMooring + lineSeparator;
                    logMessage += "\t\tShips on queue  : " + shipsOnQueue + lineSeparator;
                    totalShipsOnMooring += shipsOnMooring;
                    totalShipsOnQueue += shipsOnQueue;
                }
                logMessage += "\tTotal ships on mooring: " + totalShipsOnMooring + lineSeparator;
                logMessage += "\tTotal ships on queue  : " + totalShipsOnQueue + lineSeparator;
            }
            logMessage += messageSeparator + lineSeparator + lineSeparator;

            // log message to a file
            infoLogger.log(logMessage);

            // log information to the console to notify that information has been logged
            systemLogger.log("Info logged to file");
        }
    }
}
