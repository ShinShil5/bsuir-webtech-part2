package lab2.models.system;

/**
 * model that describes rules that has broken by the ship
 */
public class ShipBreakRules {
    // ship id
    public int shipId;

    // amount of the rules that has been broken by the ship with id shipId
    public int breakRulesAmount;
}
