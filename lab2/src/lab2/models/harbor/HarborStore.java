package lab2.models.harbor;

import lab2.models.Good;

import java.util.ArrayList;
import java.util.List;

/**
 * store for goods
 */
public class HarborStore {
    public List<Good> goods;

    public HarborStore() {
        this.goods = new ArrayList<>();
    }

    /**
     * thread safe adding good to the list
     * @param good
     */
    public synchronized void putGood(Good good) {
        this.goods.add(good);
    }
}
