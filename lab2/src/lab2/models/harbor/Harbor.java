package lab2.models.harbor;

import lab2.inout.LoggerFactory;
import lab2.models.system.SeaSystem;

import java.util.List;


/**
 * harbor model contains store and piers
 */
public class Harbor {
    // store for the goods
    public HarborStore store;
    // list of piers
    public List<BasePier> piers;

    // system where harbor has been registered
    private SeaSystem seaSystem;
    // logger factory for base pier initialization
    private LoggerFactory loggerFactory;

    /**
     * default constructor
     * @param seaSystem system where harbor has been registered
     */
    public Harbor(SeaSystem seaSystem) {
        store = new HarborStore();
        this.seaSystem = seaSystem;
        this.loggerFactory = LoggerFactory.getInstance();
        piers = List.of(
            getPier(), getPier()
        );
    }

    /**
     * builder for pier
     * @return new instance of base pier - synchronised or semaphore version
     */
    private BasePier getPier() {
        var pier = new BasePier(seaSystem, this);
        var pierLogger = loggerFactory.getPierLogger(pier);
        pier.setLogger(pierLogger);

        return pier;
    }
}
