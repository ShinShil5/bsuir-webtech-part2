package lab2.models.harbor;

import lab2.config.AppConstants;
import lab2.config.IAppConstants;
import lab2.config.ThreadSyncMode;
import lab2.inout.ILogger;
import lab2.inout.LoggerFactory;
import lab2.models.Ship;
import lab2.models.system.Mooring;
import lab2.models.system.SeaSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * base pier for thread safe specific implementations: with synchronised keyword or with semaphore
 */
public class BasePier implements Runnable {
    // ship that is on the pier's mooring
    public volatile Ship activeShip;
    // ships that waiting to start mooring on the pier
    public List<Ship> shipQueue;
    // pier's harbor
    public Harbor harbor;

    // unique identifier for the pier
    private int pierId;
    // counter that is used to generate new unique identifier
    private static int nextPierId;
    // console logger for the pier
    private ILogger logger;
    // system where pier has been registered
    private SeaSystem seaSystem;
    // logger factory needs for mooring constructor
    private LoggerFactory loggerFactory;
    // queue thread waiting while pier will be free, than take the ship from the queue
    // is queue thread started
    // it should be start only if more than zero ships waiting for mooring
    private boolean queueThreadStarted;
    private IAppConstants appConstants;
    private Semaphore shipQueueSemaphore;
    private Semaphore enterSemaphore;

    /**
     * default constructor
     * @param system system where pier is registered
     * @param harbor harbor where ship is belonged
     */
    public BasePier(SeaSystem system, Harbor harbor) {
        shipQueue = new ArrayList<>();
        activeShip = null;
        this.pierId = this.nextPierId;
        this.nextPierId++;
        this.loggerFactory = LoggerFactory.getInstance();
        this.seaSystem = system;
        this.harbor = harbor;
        this.queueThreadStarted = false;
        appConstants = new AppConstants();
        shipQueueSemaphore = new Semaphore(1);
        enterSemaphore = new Semaphore(1);
    }

    // setter for the pier's logger
    public void setLogger(ILogger logger) {
        this.logger = logger;
    }

    // is ship on the mooring on the pier
    public boolean isFree() {
        var isFree = activeShip == null;

        return isFree;
    }

    // getter for pierId
    public int getId() {
        return this.pierId;
    }

    // amount of ships on the pier's queue which are waiting for mooring
    public int getShipsInQueue() {
        var shipsInQueueAmount = this.shipQueue.size();

        return shipsInQueueAmount;
    }

    /**
     * enter ship to the pier
     * @param ship ship entered to pier
     */
    public void enter(Ship ship) {
       if(appConstants.getThreadSyncMode() == ThreadSyncMode.SYNCHRONIZED) {
           this.enterSynchronised(ship);
       } else {
           this.enterSemaphore(ship);
       }
    }

    /**
     * start thread which will take ships from the queue
     */
    private void startShipQueueThread() {
        if(appConstants.getThreadSyncMode() == ThreadSyncMode.SYNCHRONIZED) {
            this.startShipQueueThreadSynchronised();
        } else {
            this.startShipQueueThreadSemaphore();
        }
    }

    /**
     * start mooring for active ship
     */
    private void startMooring() {
        // create instance of the mooring
        var mooring = new Mooring(activeShip, this, seaSystem);

        // set logger for the create mooring
        if(logger != null) {
            var mooringLogger = loggerFactory.getMooringLogger(activeShip, this);
            mooring.setLogger(mooringLogger);
        }

        // start mooring in the sea system, this will update some sea system fields
        seaSystem.startMooring(mooring);
    }

    /**
     * finish mooring for active ship
     */
    public void finishMooring() {
        // log information to the console
        log("Finish mooring for ship " + activeShip.getId());

        // make pier free for another ship
        this.activeShip = null;
    }

    /**
     * thread that takes ships waiting for mooring from the queue once the pier is free
     * updates queueThreadStarted field
     */
    @Override
    public void run() {
        // mark that thread is started, this is used to start only one thread
        this.queueThreadStarted = true;
        // repeat until any ships waiting for mooring
        while (shipQueue.size() > 0) {
            // wait while pier is busy
            while (activeShip != null) ;

            // take ship from the queue
            this.activeShip = this.shipQueue.get(0);
            this.shipQueue.remove(0);
            log("Ship " + activeShip.getId() + " has entered to the pier from the queue");

            // start mooring for ship from the queue
            this.startMooring();
        }
        // mark that thread is finished, actually thread still is not finished
        // however it will be finished in a few milliseconds
        this.queueThreadStarted = false;
        // start new queue thread, because it is possible that new ships hase entered to the queue
        // while previous line has bee executed
        this.startShipQueueThread();
    }

    // synchronised enter implementation
    private synchronized void enterSynchronised(Ship ship)  {
       this.enterCommon(ship);
    }

    // enter part that is common for synchronised enter and semaphore enter
    private void enterCommon(Ship ship) {
        // if pier is free and not ships in queue than we can start mooring immediately
        if (activeShip == null && this.shipQueue.size() == 0) {
            this.activeShip = ship;
            log("Ship " + ship.getId() + " has entered to pier");
            this.startMooring();
        } else {
            this.shipQueue.add(ship);
            log("Ship " + ship.getId() + " has been added to the queue");
            this.startShipQueueThread();
        }
    }

    // startShipQueueThread part that is common for synchronised enter and semaphore enter
    private void startShipQueueThreadCommon() {
        if(this.queueThreadStarted == false && this.shipQueue.size() > 0) {
            this.queueThreadStarted = true;
            Thread queueThread = new Thread(this);
            queueThread.start();
        }
    }

    // log information if possible
    private void log(String line) {
        if(this.logger != null) {
            this.logger.log(line);
        }
    }

    // enter semaphore implementation
    private void enterSemaphore(Ship ship) {
        try {
            this.enterSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.enterCommon(ship);
        this.enterSemaphore.release();
    }

    // startShipQueueThread synchronised implementation
    private synchronized void startShipQueueThreadSynchronised() {
       this.startShipQueueThreadCommon();
    }

    // startShipQueueThread semaphore implementation
    private void startShipQueueThreadSemaphore() {
        try {
            this.shipQueueSemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.startShipQueueThreadCommon();
        this.shipQueueSemaphore.release();
    }
}
