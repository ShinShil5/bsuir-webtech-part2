package lab2.models;

import lab2.inout.ILogger;
import lab2.models.harbor.BasePier;
import lab2.models.system.SeaSystem;

import java.util.ArrayList;
import java.util.List;

public class Ship implements Runnable {
    public List<Good> goods;
    public int priority;
    public SeaSystem seaSystem;

    private int shipId;
    private static int nextId = 1;
    private final int defaultSleepMs = 1000;
    private BasePier pier;
    private ILogger logger;

    public Ship(SeaSystem seaSystem) {
        this.seaSystem = seaSystem;
        this.shipId = nextId;
        this.goods = new ArrayList<>();
        nextId++;
    }

    public void setLogger(ILogger logger) {
        this.logger = logger;
    }

    public void swim() throws InterruptedException {
        while(this.goods.size() < 3) {
            Thread.sleep(defaultSleepMs);
            this.goods.add(new Good(1,1,1000));
            log("Swimming... Found a good");
        }
    }

    public int getExpectedMooringTime() {
        var expectedMooringTime = 0;
        for(var good : goods) {
            expectedMooringTime += good.loadTime;
        }

        return expectedMooringTime;
    }

    public int getId() {
        return this.shipId;
    }

    @Override
    public void run() {
        while(true) {
            try {
                while(true) {
                    this.swim();
                    this.log("Enter the harbor");
                    this.pier = this.seaSystem.enterPier(this);
                    while (this.pier != null);
                    if(this.getId() == 1) {
                        break;
                    }
                }
            } catch (InterruptedException e) {

            }
        }
    }

    public void leavePier() {
        this.pier = null;
        this.log("Leave the harbor");
    }

    private void log(String line) {
        if(this.logger != null) {
            this.logger.log(line);
        }
    }
}
