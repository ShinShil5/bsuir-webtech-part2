package lab2.models;

/**
 * model that describes goods founded by the ships
  */
public class Good {
    // good urgency
    public int urgency;
    // good priority
    public int priority;
    // expected time for transferring the good from a ship to a store
    public int loadTime;

    /**
     * default constructor
     * @param urgency
     * @param priority
     * @param loadTime
     */
    public Good(int urgency, int priority, int loadTime) {
        this.urgency = urgency;
        this.priority = priority;
        this.loadTime = loadTime;
    }
}
